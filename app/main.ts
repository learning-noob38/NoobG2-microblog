// Imports for loading & configuring the in-memory api
import {provide} from '@angular/core';
import {XHRBackend} from '@angular/http'; 

import {InMemoryBackendService, SEED_DATA} from 'angular2-in-memory-web-api';

// The usual bootstrapping code
import {bootstrap} from '@angular/platform-browser-dynamic';
import {HTTP_PROVIDERS} from '@angular/http';

import {AppComponent} from './app.component';

bootstrap(AppComponent, [
    HTTP_PROVIDERS
]);