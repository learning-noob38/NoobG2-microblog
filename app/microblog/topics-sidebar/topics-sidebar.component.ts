import {Component} from '@angular/core';
import {Router, RouteParams} from '@angular/router-deprecated';

import {Http, HTTP_PROVIDERS} from '@angular/http';

import {MicroblogService} from '../microblog.service';
import {Topic} from '../topics/topic';
import {Page} from '../pages/page';

@Component({
    selector: 'topics-sidebar',
    templateUrl: 'app/microblog/topics-sidebar/topics-sidebar.component.html',
    providers: [MicroblogService]
})

export class SidebarComponent{
    
    topics: Topic[] = [];
    error: any;
    title = 'Topics Sidebar Component';

    constructor(
        private router: Router,
        private http: Http,
        private microblogService: MicroblogService
    ) { }

    ngOnInit() {
        this.getTopics();
    }

    getTopics() {
        this.microblogService.getTopics().then(
            topics => this.topics = topics
        );
    }

    gotoTopicDetail(topic: Topic){
        let link = ['Topic', {id: topic.TopicID}];
        this.router.navigate(link);
    }

    private handleError(error: any) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}