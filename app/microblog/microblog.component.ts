import {Component, Input, Output, OnInit} from '@angular/core';
import {Router} from '@angular/router-deprecated';

import {Http, HTTP_PROVIDERS} from '@angular/http';
import {Page} from './pages/page';
import {Topic} from './topics/topic';
import {MicroblogService} from './microblog.service'

@Component({
    selector: 'microblog',
    templateUrl: 'app/pages/pages.component.html',
    styleUrls: ['app/pages/pages.component.css'],
    providers: [MicroblogService]
})

export class MicroblogComponent implements OnInit {
    
    topics: Topic[] = [];
    error: any;
    title = 'Pages Component';

    constructor(
        private router: Router,
        private http: Http,
        private microblogService: MicroblogService
    ) { }

    ngOnInit() {
        this.microblogService.getTopics();
    }
}