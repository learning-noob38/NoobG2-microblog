import {Component} from '@angular/core';

@Component({
    selector: 'landing',
    template: `
            <h2>Welcome to N(oob)G2-microblog!!</h2>
            <p>This is just a landing page for testing purposes</p>
        `
})

export class LandingComponent{}