import {Component, OnInit, Input} from '@angular/core';
import {MicroblogService} from '../microblog.service';

import {Router, RouteParams} from '@angular/router-deprecated';

import {Page} from './page';


@Component({
    selector: 'page',
    templateUrl: 'app/microblog/pages/page.component.html',
    styleUrls: ['app/microblog/pages/page.component.css'],
    providers: [MicroblogService]
})

export class PageComponent implements OnInit{
    page: Page = new Page();

    constructor(
        private microblogService: MicroblogService,
        private router: Router,
        private routeParams: RouteParams
    ){}

    ngOnInit(){
        
        if (this.routeParams.get('id') !== null) {
            let id = +this.routeParams.get('id');
            this.microblogService.getPageByID(id)
            .then(page => this.page = page);
            
        } else {
            // Todo: Do something else, maybe :)
        }
    }
}