export class Page {
    PageID: number;
    PageName: string;
    PageHtml: string;
    TopicID: number;
    PageDescription: string;
    DateCreated: Date;
}