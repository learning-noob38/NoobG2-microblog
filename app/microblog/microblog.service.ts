import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http'

import {Page} from './pages/page';
import {Topic} from './topics/topic';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class MicroblogService {

    //private apiDomain = 'http://localhost:8626'
    private apiDomain = "http://herpaderpadoo-001-site1.etempurl.com"
    private pagesUrl = this.apiDomain + '/api/page';
    private topicsUrl = this.apiDomain + '/api/topic';
    constructor(private http: Http) { }

    getTopics(): Promise<Topic[]>{
        return this.http.get(this.topicsUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getPages(): Promise<Page[]> {
        return this.http.get(this.pagesUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getPageByID(id: number): Promise<Page> {
        return this.http.get(this.pagesUrl + '/' + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getTopicPages(id: number): Promise<Page[]>{
        
        return this.http.get(this.topicsUrl + '/' + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}