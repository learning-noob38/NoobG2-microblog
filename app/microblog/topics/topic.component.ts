import {Component, OnInit} from '@angular/core';
import {Page} from '../pages/page';
import {MicroblogService} from '../microblog.service';
import {Router, RouteParams} from '@angular/router-deprecated';

@Component({
    selector: 'topic',
    templateUrl: 'app/microblog/topics/topic.component.html',
    styleUrls: ['app/microblog/topics/topic.component.css'],
    providers: [MicroblogService]
})

export class TopicComponent {
    pages: Page[];

    constructor(
        private microblogService: MicroblogService,
        private router: Router,
        private routeParams: RouteParams
    ) { };

    ngOnInit() {
        if (this.routeParams.get('id') !== null) {
            var id = +this.routeParams.get('id')
            this.getTopicPages(id);
        } else {
            this.getAllPages();
        }
    }
    getAllPages() {
        this.microblogService.getPages()
            .then(pages => this.pages = pages);
    }
    getTopicPages(id: number) {
        this.microblogService.getTopicPages(id)
            .then(pages => this.pages = pages);
    }
    gotoPage(page: Page){
        let link = ["Page", {id: page.PageID}];
        this.router.navigate(link);
    }
}