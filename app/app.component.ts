import {Component} from '@angular/core';
import {RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from '@angular/router-deprecated';


import {TopicComponent} from './microblog/topics/topic.component';
import {PageComponent} from './microblog/pages/page.component';
import {LandingComponent} from './microblog/landing/landing.component';

import {SidebarComponent} from './microblog/topics-sidebar/topics-sidebar.component';

@Component({
    selector: 'my-app',
    templateUrl: 'app/app.component.html',
    styleUrls: ['app/app.component.css'],
    directives: [ROUTER_DIRECTIVES, SidebarComponent],
    providers: [
        ROUTER_PROVIDERS
    ]
})

@RouteConfig([
    {
        path: '/',
        name: 'Landing',
        component: LandingComponent,
        useAsDefault: true
    },
    {
        path: '/topic/:id',
        name: 'Topic',
        component: TopicComponent
    },

    {
        path: '/page/:id',
        name: 'Page',
        component: PageComponent
    }
])

export class AppComponent {
    title = 'Microblog';
}